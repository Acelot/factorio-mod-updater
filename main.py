import asyncio
from pathlib import Path
import urllib3
import json
import zipfile
import rich
from packaging.version import Version
import concurrent.futures
import asyncio

from rich.progress import Progress

with open("./settings.json", "rt") as fd:
    settings = json.load(fd)

settings_mods_folder = "Factorio Mods Folder"
settings_download_end_points = "Mod Download Endpoints"
settings_mod_info_endpoint = "Mod Info Endpoint"

for setting in [settings_mods_folder, settings_mod_info_endpoint, settings_download_end_points]:
    if not settings[setting]:
        raise RuntimeError(F"Error: Invalid setting: {setting}")

INVALID_DATA_FOLDER = Path("./Invalid data")
FACTORIO_MODS_FOLDER = settings[settings_mods_folder]
mod_info_endpoint = settings[settings_mod_info_endpoint]
mod_download_endpoints = settings[settings_download_end_points]

if not FACTORIO_MODS_FOLDER.exists():
    RuntimeError("Couldn't find Factorio mods folder. Exiting")

fake_user_agent = "Mozilla/5.0 (X11; Linux x86_64; rv:123.0) Gecko/20100101 Firefox/123.0"
user_agent = fake_user_agent
headers_json_request = {
    'User-Agent': user_agent,
    'Accept': '*/*',
    'Accept-Language': 'en-US,en;q=0.5',
    'Accept-Encoding': 'gzip, deflate, br',
    'DNT': '1',
    'Sec-GPC': '1',
    'Connection': 'keep-alive',
    'Sec-Fetch-Dest': 'empty',
    'Sec-Fetch-Mode': 'no-cors',
    'Sec-Fetch-Site': 'same-origin',
    'Pragma': 'no-cache',
    'Cache-Control': 'no-cache',
    "Upgrade-Insecure-Requests": '1'
}
headers_zip_download_request = {

    'User-Agent': user_agent,
    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/jxl,image/webp,*/*;q=0.8',
    'Accept-Language': 'en-US,en;q=0.5',
    'Accept-Encoding': 'gzip, deflate, br',
    'DNT': '1',
    'Sec-GPC': '1',
    'Connection': 'keep-alive',
    'Upgrade-Insecure-Requests': '1',
    'Sec-Fetch-Dest': 'document',
    'Sec-Fetch-Mode': 'navigate',
    'Sec-Fetch-Site': 'same-site',
    'Sec-Fetch-User': '?1',
    'TE': 'trailers',
    'Pragma': 'no-cache',
    'Cache-Control': 'no-cache'
}


http = urllib3.PoolManager()


def get_current_version():
    with (FACTORIO_MODS_FOLDER.parent / "data/base/info.json").open() as fd:
        return json.load(fd)["version"]


def read_zip_info_file(zip_file_path):
    with zipfile.ZipFile(zip_file_path, 'r') as zip_file:
        info_json_path = Path(zip_file.namelist()[0]).parts[0] + "/info.json"
        # info_json_path = next(filter(lambda file_path: "info.json" in file_path, zip_file.namelist()))
        with zip_file.open(info_json_path) as json_file:
            # Load JSON data
            return json.load(json_file)


def download_json(url, mod_name):
    # Send the GET request
    response = http.request('GET', F"{url}{mod_name}", headers=headers_json_request)

    # Check if the request was successful (status code 200)
    if response.status == 200:
        # Decode the gzip-encoded content if necessary
        content = response.data.decode('utf-8') if response.headers.get('Content-Encoding') == 'gzip' else response.data

        # Parse the JSON content
        try:
            json_data = json.loads(content)
            return json_data
        except json.JSONDecodeError as e:
            print(f"Error parsing JSON: {e}")
            return None
    else:
        print(f"HTTP request failed with status code {response.status}")
        return None


def iterate_mods():
    for file in FACTORIO_MODS_FOLDER.iterdir():
        if file.suffix == ".zip":
            mod_json = read_zip_info_file(file)
            yield mod_json["name"], Version(mod_json["version"])


def download_zip_file(url, destination: Path):
    with http.request('GET', url, preload_content=False,
                      headers=headers_zip_download_request) as resp, destination.open(mode='wb') as out_file:
        if resp.status == 200:
            out_file.write(resp.read())
        else:
            raise RuntimeError(F"Bad http response: {resp.status}")


def download_mod(mod_name, mod_verion, save_path: Path):
    download_url_path = F"/{mod_name}/{mod_verion}.zip"
    for (endpoint_name, endpoint) in mod_download_endpoints:
        print(F"Using endpoint {endpoint_name}")
        try:
            download_zip_file(endpoint + download_url_path, save_path / F"{mod_name}_{mod_verion}.zip")
            vaildate_mod_zip(FACTORIO_MODS_FOLDER / mod_name_to_zip_string(mod_name, mod_verion))
            return
        except RuntimeError as runtime_error:
            try:  # Move to invalid downloaded data folder for future reference & debugging
                (FACTORIO_MODS_FOLDER / mod_name_to_zip_string(mod_name, mod_verion)).replace(
                    INVALID_DATA_FOLDER / mod_name_to_zip_string(mod_name, mod_verion))
            except FileNotFoundError:
                pass
            print(runtime_error)
            continue
    raise RuntimeError("Error: All endpoints failed to download the mod!")


def mod_name_to_zip_string(mod_name, version):
    return F"{mod_name}_{version}.zip"


def get_relevant_change_log(change_log_raw, curret_version):
    change_log_list = change_log_raw.split(
        '---------------------------------------------------------------------------------------------------')
    relevant_change_logs = []
    version_string = F"Version: {curret_version}"
    for change_log_by_version in change_log_list:

        if version_string not in change_log_by_version:
            relevant_change_logs.append(change_log_by_version)
        else:
            return relevant_change_logs

    return relevant_change_logs


async def check_mod(mod_name, current_mod_version):
    # rich.print(F"Checking: {mod_name}, current version: {current_mod_version}")
    try:
        json_result = download_json(mod_info_endpoint, mod_name)
    except RuntimeError as runtime_error:
        print(runtime_error)
        print("Couldn't find mod info.")
        return

    latest_version = Version(json_result['releases'][-1]['version'])
    if latest_version > current_mod_version:
        rich.print(F"Mod: \"{mod_name}\" out of date, updating from {current_mod_version} to {latest_version}")
        try:
            download_mod(mod_name, latest_version, FACTORIO_MODS_FOLDER)
            for change_log_version in get_relevant_change_log(json_result['changelog'], current_mod_version):
                rich.print(change_log_version)
            try:
                outdated_mod_file_path = Path(
                    FACTORIO_MODS_FOLDER / mod_name_to_zip_string(mod_name, current_mod_version))
                outdated_mod_file_path.unlink()
                return (latest_version, mod_name)
            except:
                print("Failed to remove: ", outdated_mod_file_path)
        except RuntimeError as runtime_error:
            print(runtime_error)
            print("Failed to download mod.")


def vaildate_mod_zip(path: Path):
    if zipfile.is_zipfile(path):
        return
    else:
        raise RuntimeError("Downloaded file is not a zip file.")


def run_async_function_in_process(item):
    loop = asyncio.new_event_loop()
    asyncio.set_event_loop(loop)
    (mod_name, current_mod_version) = item
    result = loop.run_until_complete(check_mod(mod_name, current_mod_version))
    loop.close()
    return result


async def main():
    items = [(mod_name, current_mod_version) for (mod_name, current_mod_version) in iterate_mods()]
    with concurrent.futures.ProcessPoolExecutor() as executor:

        total_items = len(items)

        with Progress() as progress:
            task_total_progress = progress.add_task("[cyan]Updating Mods...", total=total_items)

            for result in executor.map(run_async_function_in_process, items):

                if result:
                    (latest_version, mod_name) = result
                    rich.print(F"Updated {mod_name} to version {latest_version}")
                progress.update(task_total_progress, advance=1)


if __name__ == "__main__":

    asyncio.run(main())
