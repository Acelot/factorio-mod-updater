# About
A Factorio mod updater script.

Factorio includes a build-in updater, which requires a restart once done to reinitialize the mods.
This script updates all of your mods beforehand, so you do not need to restart saving you precious time in case you have a large mod pack.

# Usage

Fill in `settings.json`
```
Factorio Mods Folder: The Factorio mods folder.
Mod Download Server: End point where the mods are downloaded from, e.g. for mod "mod_name" with version "mod_version" will attempt to download https://domain.com/mod_name/mod_version.zip
Mod Info Server: This should end with `mods/modinfo?id=`, e.g. https://domain.com/factorio/mods/modinfo?id=
```
You'll have to provide end-points yourself as it is against the EULA to have it provided pre-configured.

Run: `python3 main.py`

\pagebreak

## Screenshot

![Example Usage](Usage.png){height=70%}

\pagebreak

# Note

THERE IS NO WARRANTY FOR THE PROGRAM, TO THE EXTENT PERMITTED BY APPLICABLE LAW. EXCEPT WHEN OTHERWISE STATED IN WRITING THE COPYRIGHT HOLDERS AND/OR OTHER PARTIES PROVIDE THE PROGRAM “AS IS” WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE ENTIRE RISK AS TO THE QUALITY AND PERFORMANCE OF THE PROGRAM IS WITH YOU. SHOULD THE PROGRAM PROVE DEFECTIVE, YOU ASSUME THE COST OF ALL NECESSARY SERVICING, REPAIR OR CORRECTION.
